"""Test pythia.misc module."""

import numpy as np
import pytest
from pythia.cartesian_product import cartesian_product


def test_cartesian_product() -> None:
    """Test build of cartesian product."""
    # empty list raises error
    with pytest.raises(ValueError):
        _ = cartesian_product() == []

    # one array in list returns reshaped array
    array = np.arange(10)
    assert np.all(cartesian_product(array) == array.reshape(-1, 1))

    # only one point in each dimension
    array = np.arange(10).reshape(-1, 1)
    assert np.all(cartesian_product(*array) == np.arange(10).reshape(1, -1))

    # single array is treated as n-D list with one point each
    # (same result as previous test)
    array = np.arange(10)
    assert np.all(cartesian_product(*array) == array.reshape(1, -1))

    # multiple arrays of different length (standard usecase)
    result = np.array([[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2]])
    assert np.all(cartesian_product(np.arange(2), np.arange(3)) == result)
