"""Test pythia.basis module."""

from unittest.mock import MagicMock

import numpy as np
import pythia.least_squares_sampler as lssampler
from pythia.basis import Function1D
from pythia.sampler import Sampler
from scipy.integrate import quad


def test_OWLSUnivariateSampler() -> None:
    """Test univariate wls sampler.

    .. note::
        This currently only tests uniform WLS sampling.
    """

    param = MagicMock()
    param.distribution = "uniform"
    param.domain = [-1, 1]
    s = lssampler.OWLSUnivariateSampler(param, deg=5, tsa=True)
    # define normalized Legendre basis polynomials
    basis = [
        np.polynomial.legendre.Legendre([0] * j + [1], param.domain) for j in range(6)
    ]
    scales = [
        quad(lambda x: 0.5 * p(x) ** 2, param.domain[0], param.domain[1])[0]
        for p in basis
    ]
    basis = [p / np.sqrt(scale) for (p, scale) in zip(basis, scales)]

    # test general properties
    assert isinstance(s, Sampler)
    assert isinstance(s.domain, np.ndarray)
    assert s.dimension == 1
    assert s.mass == 1
    assert np.abs(s.mean - 0.0) < 1e-10
    assert np.abs(s.var - 0.4965034965034964) < 1e-10
    assert np.abs(s.std - np.sqrt(0.4965034965034964)) < 1e-10
    assert np.abs(s.maximum - 3) < 1e-10

    # test weight
    xs = np.random.uniform(param.domain[0], param.domain[1], 100)
    expected = 6 / np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0)
    vals = s.weight(xs)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test pdf
    xs = np.random.uniform(param.domain[0], param.domain[1], 100)
    vals = s.pdf(xs)
    expected = 1 / 12 * np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test log-pdf
    xs = np.random.uniform(param.domain[0], param.domain[1], 100)
    vals = s.log_pdf(xs)
    expected = np.log(np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0) / 12)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test grad log-pdf
    # Note: not yet implemented.

    # test hessian log-pdf
    # Note: not yet implemented.

    # test _compute_trial_sampler
    xs = np.random.uniform(param.domain[0], param.domain[1], 100)
    trial_sampler, bulk = s._compute_trial_sampler()
    assert np.all(bulk * trial_sampler.pdf(xs) > s.pdf(xs))

    # test sampling
    # Note: There is no reasonable way to reliably test if moments of
    #       distribution are correct (without drawing a lot of samples)
    assert s.sample((100, 1)).shape == (100, 1)


def test_WLSSampler() -> None:
    """Test weighted least-squares sampler."""
    param = MagicMock()
    param.distribution = "uniform"
    param.domain = [-1, 1]

    # define normalized Legendre basis polynomials
    basis = [
        np.polynomial.legendre.Legendre([0] * j + [1], param.domain) for j in range(6)
    ]
    scales = [
        quad(lambda x: 0.5 * p(x) ** 2, param.domain[0], param.domain[1])[0]
        for p in basis
    ]
    basis = [p / np.sqrt(scale) for (p, scale) in zip(basis, scales)]
    m_basis = []
    for p in basis:
        for q in basis:

            def func(x: np.ndarray, p: Function1D = p, q: Function1D = q) -> np.ndarray:
                return p(x[:, 0]) * q(x[:, 1])

            m_basis.append(func)
    s = lssampler.OWLSSampler([param, param], m_basis, tsa=True)

    # test general properties
    assert isinstance(s, Sampler)
    assert isinstance(s.domain, np.ndarray)
    assert s.dimension == 2
    assert s.mass == 1
    assert np.abs(s.maximum - 9) < 1e-10

    # Note: mean and cov not implemented

    # test weight
    xs = np.random.uniform(param.domain[0], param.domain[1], (100, 2))
    expected = 6**2 / np.prod(
        np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0), axis=1
    )
    vals = s.weight(xs)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test pdf
    xs = np.random.uniform(param.domain[0], param.domain[1], (100, 2))
    vals = s.pdf(xs)
    expected = (
        1 / 12**2 * np.prod(np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0), axis=1)
    )
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test log-pdf
    xs = np.random.uniform(param.domain[0], param.domain[1], (100, 2))
    vals = s.log_pdf(xs)
    expected = np.sum(
        np.log(np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0)), axis=1
    ) - np.log(12**2)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test grad log-pdf
    # Note: not yet implemented.

    # test hessian log-pdf
    # Note: not yet implemented.

    # test sampling
    # Note: There is no reasonable way to reliably test if moments of
    #       distribution are correct (without drawing a lot of samples)
    assert s.sample((100, 1)).shape == (100, 1, 2)


def test_WLSTensorSampler() -> None:
    """Test weighted least-squares tensor product sampler."""
    param = MagicMock()
    param.distribution = "uniform"
    param.domain = [-1, 1]
    s = lssampler.OWLSTensorSampler([param, param], [5, 5], tsa=True)

    # define normalized Legendre basis polynomials
    basis = [
        np.polynomial.legendre.Legendre([0] * j + [1], param.domain) for j in range(6)
    ]
    scales = [
        quad(lambda x: 0.5 * p(x) ** 2, param.domain[0], param.domain[1])[0]
        for p in basis
    ]
    basis = [p / np.sqrt(scale) for (p, scale) in zip(basis, scales)]

    # test general properties
    assert isinstance(s, Sampler)
    assert isinstance(s.domain, np.ndarray)
    assert s.dimension == 2
    assert s.mass == 1
    assert np.abs(s.maximum - 9) < 1e-10
    assert np.linalg.norm(s.mean - [0, 0]) < 1e-12
    var = 0.4965034965034964
    assert np.linalg.norm(s.cov - np.diag([var, var])) < 1e-12

    # test weight
    xs = np.random.uniform(param.domain[0], param.domain[1], (100, 2))
    expected = 6**2 / np.prod(
        np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0), axis=1
    )
    vals = s.weight(xs)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test pdf
    xs = np.random.uniform(param.domain[0], param.domain[1], (100, 2))
    vals = s.pdf(xs)
    expected = (
        1 / 12**2 * np.prod(np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0), axis=1)
    )
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test log-pdf
    xs = np.random.uniform(param.domain[0], param.domain[1], (100, 2))
    vals = s.log_pdf(xs)
    expected = np.sum(
        np.log(np.sum([np.abs(p(xs)) ** 2 for p in basis], axis=0)), axis=1
    ) - np.log(12**2)
    assert vals.shape == (100,)
    assert np.max(np.abs(vals - expected)) < 1e-12

    # test grad log-pdf
    # Note: not yet implemented.

    # test hessian log-pdf
    # Note: not yet implemented.

    # test sampling
    # Note: There is no reasonable way to reliably test if moments of
    #       distribution are correct (without drawing a lot of samples)
    assert s.sample((100, 1)).shape == (100, 1, 2)
