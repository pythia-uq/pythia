# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join("..", "..", "pythia")))

# -- Mockups for standard python packages ------------------------------------

# Python packages that are not accessible in the standard python version need
# to be imported during the generation of the sphinx-doc. However, the code
# is never executed. Hence it suffices to mock these modules.
# NOTE: This handles the autodoc warnings for unknown modules.
autodoc_mock_imports = ["numpy" "scipy" "psutil"]


# -- Project information -----------------------------------------------------

project = "PyThia"
copyright = "2021-24, Nando Hegemann"
author = "Nando Hegemann"

# The full version, including alpha/beta/rc tags
release = "01.02.2021"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.napoleon",  # numpy- & google-style docstrings (load before autodoc)
    "sphinx.ext.autodoc",
    "sphinx_autodoc_typehints",  # requires: pip install sphinx-autodoc-typehints
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']
html_static_path = []

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = "../../logo/logo_full_mixed.png"

# The name of an image file (relative to this directory) to use as a favicon of
# the docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = "../../logo/logo_favicon.ico"
