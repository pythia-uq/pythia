.. _tutorial-03:

Tutorial 03 - Computation of Sobol Indices
==========================================

This tutorial covers the approximation of the Sobol indices of a target function, which are used to infer information about the global parameter sensitivity of the model.
To verify the results we compute with PyThia, we use the Sobol function as the object of interest, as the Sobol indices are explicitly known for this function.
The Sobol function is given by

.. math::
   f(x) = \prod_{j=1}^{M} \frac{|4 x_j-2| + a_j}{1 + a_j}
   \qquad\mbox{for}\quad a_1,\dots,a_M \geq 0.

.. note::
   The larger :math:`a_j`, the less is the influence, i.e. the sensitivity, of parameter :math:`x_j`.

The mean of the Sobol function is :math:`\mathbb{E}[f]=1` and the variance reads

.. math::
   \operatorname{Var}[f] = \prod_{j=1}^{M} (1+c_j) - 1
   \qquad\mbox{for}\qquad
   c_j = \frac{1}{3(1+a_j)^{2}}.

With this, we can easily compute the Sobol indices by
   
.. math::
   \operatorname{Sob}_{i_1, ..., i_s} 
   = \frac{1}{\operatorname{Var}[f]}\prod_{k=1}^{s} c_{i_k}.

An implementation of the Sobol function method ``sobol_function()`` and the analytical Sobol indices method ``sobol_sc()`` is included in the complete script at the end of this tutorial.

First, we choose some values for the coefficients :math:`a_1,\dots,a_M` and with this the target function.

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 5-7,76,79,86

Additionally, we compute the analytical Sobol indices.

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 90-91

Next we define the necessary quantities for the PC expansion.
For more information see :ref:`tutorial-01`.
As stochastic parameters we need to choose uniformly distributed variables :math:`x_j` on :math:`[0,1]` according to the number of coefficients :math:`a_1,\dots,a_M`, i.e.,

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 94-97

As expansion terms we choose multivariate Legendre polynomials of total polynomial degree less then :math:`11`


.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 99-102

The last thing we need are training data.
We generate the training pairs again by an optimal weighted distribution, see :ref:`tutorial-02` for more detail.

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 108,110-113

With this, we compute the PC expansion of the ``target_function`` with PyThia

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 116

As the approximative Sobol indices can be easily derived from the PC expansion coefficients, the ``PolynomialChaos`` class computes them automatically upon initialization.
They can be accessed via ``surrogate.sobol``, which is an array ordered according to ``index_set.sobol_tuples``.
Hence it is easy to verify, if the approximation of the Sobol indices was done correctly.

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :lines: 135-144

This concludes this tutorial.

Complete Script
---------------

The complete source code listed below is located in the ``tutorials/`` subdirectory of the repository root.

.. literalinclude:: ../../../tutorials/tutorial_03.py
  :language: python
  :linenos:
