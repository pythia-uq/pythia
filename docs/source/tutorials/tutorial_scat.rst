.. _tutorial-scat:

Tutorial - Shape Reconstruction via Scatterometry
=================================================

This tutorial shows how PyThia can be used to approximate the scatterometry measurement process and conduct a global sensitivity analysis of the model.

In this experiment a silicon line grating is irradiated by an electromagnetic wave under different angles of incidence :math:`\theta` and azimuthal orientations :math:`\phi`.
The aim of this experiment is to build a surrogate for the map from the six shape parameters :math:`x = (\mathit{h}, \mathit{cd}, \mathit{swa}, \mathit{t}, \mathit{r}_{\mathrm{top}}, \mathit{r}_{\mathrm{bot}})` to the detected scattered far field of the electromagnetic wave.
The experimental setup is visualized in the image below.

.. image:: ../../img/scat_setup.png
   :alt: scat-setup

.. note::
   If you want to run the source code for this tutorial yourself, you need to download the |tutorial data|.
   This tutorial assumes that the data are located in the same directory as the source code.

Surrogate
---------

We start by loading the ``numpy`` and ``pythia`` packages.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 8-9

Generating training data from the forward problem requires solving Maxwell's equations with finite elements, which is very intricate and time consuming.
We assume the training data we rely on in this tutorial have been previously generated in an "offline" step, on a different machine using the |JCMsuite| software.
Hence we can simply load the data.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 27-29

For the simulations we assumed Beta distributed parameters in reasonable parameter domains, which is also the density we drew the samples from.
In particular, this implies that the weights are simply :math:`w_i = 1 / N`, where :math:`N` is the number of samples (i.e., :math:`10.000` in this case).
The scattered intensity signal data consist of 4 curves (2 different azimuthal angles :math:`\phi` with s- and p- polarized wave signals each) where each curve is evaluated in 45 points (angles of incidence :math:`\theta`).
The continuous curves look similar to the image below.

.. image:: ../../img/scat_forward_eval.png
   :alt: scat-forward-eval

As a first step to compute a polynomial chaos expansion, we need to define the parameters using PyThias `Parameter` class.
We can use the ``parameter.npy`` file to access the parameter information.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 41-51

Below you can find a table and an image listing the relevant parameter information.

.. csv-table:: Parameter Information
   :header: "#", "Name", "distribution", ":math:`\\alpha`", ":math:`\\beta`"
   :widths: auto

   "1", ":math:`\mathit{h}`", ":math:`\mathrm{Beta}_{\alpha,\beta}([43, 53])`", ":math:`10.0`", ":math:`9.67`"
   "2", ":math:`\mathit{cd}`", ":math:`\mathrm{Beta}_{\alpha,\beta}([22, 28])`", ":math:`10.0`", ":math:`7.76`"
   "3", ":math:`\mathit{swa}`", ":math:`\mathrm{Beta}_{\alpha,\beta}([84, 90])`", ":math:`10.0`", ":math:`10.13`"
   "4", ":math:`\mathit{t}`", ":math:`\mathrm{Beta}_{\alpha,\beta}([4, 6])`", ":math:`10.0`", ":math:`11.29`"
   "5", ":math:`\mathit{r}_{\mathrm{top}}`", ":math:`\mathrm{Beta}_{\alpha,\beta}([8, 13])`", ":math:`10.0`", ":math:`11.10`"
   "6", ":math:`\mathit{r}_{\mathrm{bot}}`", ":math:`\mathrm{Beta}_{\alpha,\beta}([3, 7])`", ":math:`10.0`", ":math:`12.35`"

.. image:: ../../img/scat_param_dist.png
   :alt: scat-param-dist

Next, we split the available data into a training and a test set, using roughly :math:`8.000` data points for the training.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 65-75

Now that we have our parameters defined and the training data in place, we only need to define which expansion terms we want to include into our expansion.
In this case, we choose a simplex index set to bound the total polynomial degree of the expansion by four.
This leaves us with :math:`210` polynomial chaos terms in our expansion.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 78-79

Finally, we have all the ingredients to compute our polynomial chaos surrogate, which requires only one line of code.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 89

Evaluating the surrogate on our test set with

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :lines: 91

allows us compute an empirical approximation of the :math:`L^2`-error (MSE), which is approximately at :math:`5*10^{-3}`.
This is ok, since the measurement error of the observation we will investigate for the inverse problem will be at least one order of magnitude larger.

Sensitivity analysis
--------------------
 
There are essentially two different types of sensitivity analyses, local and global.
Local sensitivity analysis typicalls considers a change in the signal by a small variation in the input around a specific point.
Given the polynomial chaos surrogate, computation of the local sensitivities is of course very easy as derivatives of the polynomial are given analytically.
To compute the local sensitivities of the second parameter (:math:`\mathit{cd}`) in all points of our test set, i.e. :math:`\partial/\partial x_{\mathit{cd}}\ f(x_{\mathrm{test}})`, we can use the optional ``partial`` argument of the ``eval()`` function

.. code-block:: python
   
   local_sensitivity = surrogate.eval(x_test, partial=[0, 1, 0, 0, 0, 0])

However, computing local sensitivities yields only a very limited image of the signal dependency on change in the individual parameter or combinations thereof.
When it comes to estimating whether the reconstruction of the hidden parameters will be possible it is often more reliable to consider the overall dependence of the signal on the parameters.
There are various types of global sensitivity analysis methods, but essentially all of them rely on integrating the target function over the high-dimensional parameter space, which is typically not feasible.
The polynomial chaos expansion, however, is closely connected to the so called Sobol indices, which allows us to compute these indices by squaring and summing different subsets of our expansion coefficients.
The Sobol coefficients are automatically computed with the polynomial chaos approximation and can be accessed via the ``surrogate.sobol`` attribute, which is ordered according to the list of Sobol tuples in ``index_set.sobol_tuples``.
As the signal data consist of multiple points on different curves, we get the Sobol indices for each point of our signal.
As this is hard to visualize however, we can have a look on the maximal value each Sobol index takes over the signal points.
The 10 most significant Sobol indices are listed in the table below.

.. csv-table:: Most relevant Sobol indices
   :header: "#", "Sobol tuple", "Parameter combinations", "Sobol index value (max)"
   :widths: auto

   "1",  ":math:`(2,)`",      ":math:`(\mathit{cd},)`",                                         ":math:`0.9853`"
   "2",  ":math:`(4,)`",      ":math:`(\mathit{t},)`",                                          ":math:`0.8082`"
   "3",  ":math:`(1,)`",      ":math:`(\mathit{h},)`",                                          ":math:`0.3906`"
   "4",  ":math:`(3,)`",      ":math:`(\mathit{swa},)`",                                        ":math:`0.2462`"
   "5",  ":math:`(2, 5)`",    ":math:`(\mathit{cd}, \mathit{r}_{\mathrm{top}})`",               ":math:`0.0332`"
   "6",  ":math:`(5,)`",      ":math:`(\mathit{r}_{\mathrm{top}},)`",                           ":math:`0.0240`"
   "7",  ":math:`(3, 5)`",    ":math:`(\mathit{swa}, \mathit{r}_{\mathrm{top}})`",              ":math:`0.0137`"
   "8",  ":math:`(1, 2)`",    ":math:`(\mathit{h}, \mathit{cd})`",                              ":math:`0.0065`"
   "9",  ":math:`(6,)`",      ":math:`(\mathit{r}_{\mathrm{bot}},)`",                           ":math:`0.0059`"
   "10", ":math:`(2, 3, 5)`", ":math:`(\mathit{cd}, \mathit{swa}, \mathit{r}_{\mathrm{top}})`", ":math:`0.0030`"

We see that all single parameter Sobol indices are among the top ten, implying that changes in single parameters are among the most influential.
Moreover, looking at the actual (maximal) values, we can assume that the first 4 parameters (:math:`\mathit{h}`, :math:`\mathit{cd}`, :math:`\mathit{swa}`, :math:`\mathit{t}`) can be reconstructed very well, whereas the sensitivities of the corner roundings :math:`\mathit{r}_{\mathrm{top}}` and :math:`\mathit{r}_{\mathrm{bot}}` imply that we will get large uncertainties in the reconstruction.

This concludes the tutorial.

.. |tutorial data| raw:: html

    <a href="https://zenodo.org/record/8168617" target="_blank">tutorial data</a>

.. |JCMsuite| raw:: html

    <a href="https://www.docs.jcmwave.com/JCMsuite/html/index.html" target="_blank">JCMsuite</a>

Complete Script
---------------

The complete source code listed below is located in the ``tutorials/`` subdirectory of the repository root.

.. literalinclude:: ../../../tutorials/tutorial_scat.py
  :language: python
  :linenos:
