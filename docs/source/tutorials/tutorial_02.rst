.. _tutorial-02:

Tutorial 02 - Approximation of n-D functions with Polynomial Chaos
==================================================================

This tutorial covers the extension of :ref:`tutorial-01` to an arbitrary number of stochastic parameters as input for the target function.

For reasons of simplicity, we consider the real valued function

.. math::
   f(x) = -\sin(4\pi x_1) \sin(3\pi x_2) + 2.

as the target function throughout this tutorial, i.e.,

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :lines: 3-5,7,14-15

First, we define the stochastic input parameter :math:`x=(x_1,x_2)` with :math:`x\sim\mathcal{U}([0,1]^2)`.

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :lines: 21-23

Next, we need to specify which terms the PC expansion should include.
For this, we need the `IndexSet` class of PyThia.
We will just take all the expansion terms from the zeroth up to a certain polynomial degree, but for different degrees in each component.

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :lines: 25-27

What remains is to generate training data for the PC expansion.
Here, we use a specific strategy to generate samples that are optimal for training.
For more detail on the optimality of the sampling strategy see the work of `Cohen & Migliorati (2017) <https://smai-jcm.centre-mersenne.org/item/SMAI-JCM_2017__3__181_0/>`_.
Try and see how the surrogate changes, if you use a different number of samples or a different sampling strategy.

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :lines: 34,36-39

We assembled all the data we need to compute our surrogate and can finally use the ``PolynomialChaos`` class of the PyThia.chaos method.

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :lines: 42

The ``PolynomialChaos`` object we just created can do a lot of things, but for the moment we are only interested in the approximation of our function.
Let us generate some testing data to see how good our approximation is.

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :lines: 34,36-39

.. note::
   For testing, we choose sample realizations drawn according to the distribution of the parameters, not with respect to the weighted Least-Squares distribution we used to generate the training data.

This concludes the second tutorial.
Below you find the complete script you can use to run on your own system.
This script also computes the approximation error of the PC surrogate and plots the approximation against the target.

Complete Script
---------------

The complete source code listed below is located in the ``tutorials/`` subdirectory of the repository root.

.. literalinclude:: ../../../tutorials/tutorial_02.py
  :language: python
  :linenos:
