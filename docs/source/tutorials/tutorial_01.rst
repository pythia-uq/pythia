.. _tutorial-01:

Tutorial 01 - Approximation of Functions with Polynomial Chaos
==============================================================

In this tutorial we cover the very basic usage of PyThia by approximating a vector valued function depending on one stochastic parameter.

The function we want to approximate by a polynomial chaos expansion is a simple sine in both components, i.e.,

.. math::
   f(x) = \bigl( \sin(4\pi x) + 2,\ \sin(3\pi x) + 2 \bigr).

So we define the target function first.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :lines: 3-5,7,14-16

To utilize the polynomial chaos expansion implemented in PyThia, we need to define the stochastic parameter.
For this tutorial, we consider the parameter :math:`x` to be uniformly distributed on the interval :math:`[0,1]`.
Other admissible distributions are `normal`, `gamma` and `beta`.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :lines: 21

We need to specify which terms the sparse PC expansion should include, i.e., create a multiindex set with the `IndexSet` class.
Here, we will simply limit the maximal polynomial degree and include all expansion terms with total degree smaller then the chosen degree.
The `index` module provides diverse function to generate multiindex arrays, e.g., ``tensor_set``, ``simplex_set``, ``lq_bound_set``, ``union``, ``intersection``` and ``set_difference``.
But since we only have one variable in this tutorial, we can also create a list using ``numpy``.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :lines: 26-27

Next we generate training data for the linear regression.
Here, we use the distribution specified by the parameter to generate samples.
Try and see how the surrogate changes, if you use a different number of samples or a different sampling strategy.
We also need weights for the linear regression used to compute the polynomial chaos approximation.
The integrals are approximated with a standard empirical integration rule in our case.
Thus all the weights are equal and are simply :math:`1` over the number of samples we use.
Most importantly, however, we need function evaluations.
Note that the shape has to be equal to the number of samples in the first and image dimension in the second component.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :lines: 34,36-39

Since we assembled all the data we need to compute our surrogate, we can finally use the ``PolynomialChaos`` class of the `pythia.chaos` module.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :lines: 42

.. note::
   The ``PolynomialChaos`` class expects a list of parameters to be given.

The ``PolynomialChaos`` object we just created can do a lot of things, but for the moment we are only interested in the approximation of our function.
Let us generate some testing data to see how good our approximation is.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :lines: 44,46-48

This concludes the first tutorial.

Complete Script
---------------

The complete source code listed below is located in the ``tutorials/`` subdirectory of the repository root.

.. literalinclude:: ../../../tutorials/tutorial_01.py
  :language: python
  :linenos:
