Tutorials
=========

Here you can find some tutorials that explain the basic functionality of the PyThia software package.
If you want to run the tutorials on your own machine, make sure that your have :doc:`installed PyThia <../installation>`.

Tutorials - Basics
------------------

.. toctree::
   :maxdepth: 1

   tutorial_01
   tutorial_02
   tutorial_03
   tutorial_04

Tutorials - Applications
------------------------

.. toctree::
   :maxdepth: 1

   tutorial_scat

.. |matplotlib| raw:: html

    <a href="https://matplotlib.org/" target="_blank">matplotlib</a>
