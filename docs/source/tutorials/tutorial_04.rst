.. _tutorial-04:

Tutorial 04 - Automatic choice of expansion terms
=================================================

This tutorial covers the automatic generation of sparse PC expansion indices based on a crude approximation of the Sobol indices.
To verify the results we compute with PyThia, we use the Sobol function as the object of interest, as the Sobol indices are explicitly known for this function.
The Sobol function is given by

.. math::
   f(x) = \prod_{j=1}^{M} \frac{|4 x_j-2| + a_j}{1 + a_j}
   \qquad\mbox{for}\quad a_1,\dots,a_M \geq 0.

.. note::
   The larger :math:`a_j`, the less is the influence, i.e., the sensitivity, of parameter :math:`x_j`.

The mean of the Sobol function is :math:`\mathbb{E}[f]=1` and the variance reads

.. math::
   \operatorname{Var}[f] = \prod_{j=1}^{M} (1+c_j) - 1
   \qquad\mbox{for}\qquad
   c_j = \frac{1}{3(1+a_j)^{2}}.

With this, we can easily compute the Sobol indices by
   
.. math::
   \operatorname{Sob}_{i_1, ..., i_s} 
   = \frac{1}{\operatorname{Var}[f]}\prod_{k=1}^{s} c_{i_k}.

An implementation of the Sobol function method ``sobol_function()`` and the analytical Sobol indices method ``sobol_sc()`` is included in the complete script at the end of this tutorial.

First, we choose some values for the coefficients :math:`a_1,\dots,a_M` and with this the target function.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 5-7,76,79,86

Additionally, we compute the analytical Sobol indices.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 90-91

Next we define the necessary quantities for the PC expansion.
For more information see :ref:`tutorial-01`.
As stochastic parameters we need to choose uniformly distributed variables :math:`x_j` on :math:`[0,1]` according to the number of coefficients :math:`a_1,\dots,a_M`, i.e.,

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 94-97

We want to compare the approximation results of the PC expansion using automatically generated expansion indices with a choice done by hand.
For this we compute a reference PC expansion using multivariate Legendre polynomials of total degree less then :math:`7`.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 99-101

The last thing we need are training data.
We generate the training pairs again by an optimal weighted distribution, see :ref:`tutorial-02` for more detail.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 107,109-112

With this, we can compute a reference PC expansion for our forward model with

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 115

With this out of the way we now focus on choosing a more sparse set of PC expansion indices automatically.
To fit our needs, we specify the maximal number of expansion terms we want the PC expansion to have as well as the truncation threshold.
Computing the "optimal" multiindices can then be done using ``pt.chaos.find_optimal_indices``.
To show the efficiency of the automatic multiindex computation, we choose an expansion with half the number of terms (:math:`\sim 100`) as the reference PC (:math:`\sim 200`) and set the truncation threshold to :math:`10^{-3}`.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 118-119,121-124

For a detailed explanation on the workings of ``pt.chaos.find_optimal_indices`` we refer to the module documentation.
However, we want to provide a small explanation of the ``threshold`` input.
In principle ``find_optimal_indices`` computes a very inaccurate PC expansion with far too many terms for the given amount of training data to obtain a very crude approximation of as many Sobol indices as possible.
The expansion is chosen so that at least one expansion term is computed for each Sobol index.
Depending on the ``threshold`` the function decides which Sobol indices, i.e., parameter combinations are most relevant.
This means, that the function will exclude all multiindices associated to Sobol indices that have a (combined) contribution of less then ``threshold``.
The multiindices are then distributed according to the magnitude of the crude Sobol index computation.
We use this option here only for showcasing the results later on.

Computing the PC expansion with the automatically chosen multiindices can now be done as before.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 131

What remains is to check if the approximation of the Sobol function using only half the expansion terms is comparable in both the approximation error as well as the approximate Sobol indices.
If everything went right you should see that the approximation error of both PC expansions is of the same order of magnitude and that the Sobol indices coincide where they are greater then :math:`10^{-3}`.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :lines: 142-

This concludes this tutorial.

Complete Script
---------------

The complete source code listed below is located in the ``tutorials/`` subdirectory of the repository root.

.. literalinclude:: ../../../tutorials/tutorial_04.py
  :language: python
  :linenos:
