.. image:: ../../logo/logo_full_transparent.png
   :alt: logo

|

PyThia - Uncertainty Quantification Toolbox
===========================================

The PyThia UQ toolbox uses polynomial chaos surrogates to efficiently generate a surrogate of any (parametric) forward problem.
The surrogate is fast to evaluate, allows analytical differentiation and has a built-in global sensitivity analysis via Sobol indices.
Assembling the surrogate is done non-intrusive by least-squares regression, hence only training pairs of parameter realizations and evaluations of the forward problem are required to construct the surrogate.
No need to compute any nasty interfaces for legacy code.

For more information, see the |PyThia GitLab repository|.

.. |PyThia GitLab repository| raw:: html

    <a href="https://gitlab.com/pythia-uq/pythia" target="_blank">PyThia GitLab repository</a>


Why the Name?
=============

Pythia was the title of the high priestess of the temple of Apollo in Delphi.
Hence you could say she used her prophetic abilities to quantify which was uncertain.
Moreover, the package is written in python, so...

How to cite PyThia
==================

You can cite PyThia using the following article:

.. image:: https://joss.theoj.org/papers/10.21105/joss.05489/status.svg
   :target: https://doi.org/10.21105/joss.05489

You can also create the citation using the ``CITATION.cff`` file in this repository.

License
=======

This work is dual-licensed under GNU Lesser General Public License v3.0 or later and Hippocratic License 3.0 or later.
You can choose between one of them if you use this work.

``SPDX-License-Identifier: LGPL-3.0-or-later OR Hippocratic-3.0-ECO-MEDIA-MIL``

Funding
=======

The development of PyThia UQ Toolbox vers. 1 and 2 has been funded by the German Central Innovation Program (ZIM) No. ZF4014017RR7.
The development of PyThia UQ Toolbox vers. 3 has been funded by the |EMPIR project 20IND04-ATMOC|.

.. |EMPIR project 20IND04-ATMOC| raw:: html

    <a href="https://www.atmoc.ptb.de/home/" target="_blank">EMPIR project 20IND04-ATMOC</a>

Logo
====

Access and usage information about the PyThia logo can be found under the following URL: |https://gitlab.com/pythia-uq/pythia-logo|.

.. |https://gitlab.com/pythia-uq/pythia-logo| raw:: html

    <a href="https://gitlab.com/pythia-uq/pythia-logo" target="_blank">https://gitlab.com/pythia-uq/pythia-logo</a>

Contents
========

.. toctree::
   :maxdepth: 1

   installation.rst
   tutorials/index.rst
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
