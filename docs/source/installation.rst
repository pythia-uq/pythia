.. _pythia-installation:

Installation
============

The installation of PyThia is very quick and easy.

The latest stable version of PyThia can be installed using pip

.. code-block:: sh

   pip install pythia-uq

To install PyThia from source, i.e., if you want to work with the latest (and possibly unstable) changes, simply clone the repository and run the setup script to install PyThia to any environment

.. code-block:: sh

   git clone https://gitlab.com/pythia/pythia.git
   cd pythia
   pip install .

PyThia can then be imported from any location with ``import pythia``.

