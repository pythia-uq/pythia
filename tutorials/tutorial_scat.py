"""Tutorial - Shape Reconstruction via Scatterometry."""

import numpy as np
import pythia as pt

angles = np.load("angles.npy")  # (90, 2)

# reshape
angles = np.concatenate([angles, angles], axis=0)  # (180, 2)
angle_idxs = [range(45), range(45, 90), range(90, 135), range(135, 180)]
angle_labels = [
    "$\\phi=0^\\circ$, s pol",
    "$\\phi=0^\\circ$, p pol",
    "$\\phi=90^\\circ$, s pol",
    "$\\phi=90^\\circ$, p pol",
]
print("angle info:")
print(f"    azimuth phi: {np.unique(angles[:,0])}")
print(f"    incidence theta: {np.unique(angles[:,1])}")

# load data
x_data = np.load("x_train.npy")  # (10_000, 6)
y_data = np.load("y_train.npy")  # (90, 10_000, 2)
w_data = np.load("w_train.npy")  # (10_000, )

# reshape y_data
y_data = np.concatenate(
    [y_data[:45, :, 0], y_data[:45, :, 1], y_data[45:, :, 0], y_data[45:, :, 1]], axis=0
).T  # (10_000, 180)
print("data info:")
print(f"    x_data.shape: {x_data.shape}")
print(f"    w_data.shape: {w_data.shape}")
print(f"    y_data.shape: {y_data.shape}")

# load parameters
params = []
for dct in np.load("parameter.npy", allow_pickle=True):
    params.append(
        pt.parameter.Parameter(
            name=dct["_name"],
            domain=dct["_dom"].tolist(),
            distribution=dct["_dist"],
            alpha=dct["_alpha"],
            beta=dct["_beta"],
        )
    )

print("parameter info:")
print("    parameter  dist    domain    alpha   beta")
print("    " + "-" * 42)
for param in params:
    print(
        f"    {param.name:<8}  ",
        f"{param.distribution:<5}  ",
        f"{str(np.array(param.domain, dtype=int)):<7}  ",
        f"{param.alpha:<4.2f}  ",
        f"{param.beta:<4.2f}",
    )

# split training/test dataset
split = int(0.8 * x_data.shape[0])

# training data
x_train = x_data[:split]
y_train = y_data[:split]
w_train = w_data[:split]

# test data
x_test = x_data[split:]
y_test = y_data[split:]

# define multiindex set
indices = pt.index.simplex_set(dimension=len(params), maximum=4)
index_set = pt.index.IndexSet(indices)

print("Multiindex information:")
print(f"    number of indices: {index_set.shape[0]}")
print(f"    dimension: {index_set.shape[1]}")
print(f"    maximum dimension: {index_set.max}")
print(f"    number of sobol indices: {len(index_set.sobol_tuples)}")

print("compute PC surrogate")
# run pc approximation
surrogate = pt.chaos.PolynomialChaos(params, index_set, x_train, w_train, y_train)
# test approximation error
y_approx = surrogate.eval(x_test)

# L2-L2 error
err_abs = np.sum(np.linalg.norm(y_test - y_approx, axis=1)) / y_test.shape[0]
err_rel = np.sum(np.linalg.norm((y_test - y_approx) / y_test, axis=1)) / y_test.shape[0]
print(f"    L2-L2 error: {err_abs:4.2e} (abs), {err_rel:4.2e} (rel)")

# C0-L2 error
err_abs = np.sum(np.max(np.abs(y_test - y_approx), axis=1)) / y_test.shape[0]
err_rel = (
    np.sum(np.max(np.abs(y_test - y_approx) / np.abs(y_test), axis=1)) / y_test.shape[0]
)
print(f"    L2-C0 error: {err_abs:4.2e} (abs), {err_rel:4.2e} (rel)")

# global sensitivity analysis
print("maximal Sobol indices:")
max_vals = np.max(surrogate.sobol, axis=1)
l2_vals = np.linalg.norm(surrogate.sobol, axis=1)
sobol_max = [
    list(reversed([x for _, x in sorted(zip(max_vals, index_set.sobol_tuples))]))
]
sobol_L2 = [
    list(reversed([x for _, x in sorted(zip(l2_vals, index_set.sobol_tuples))]))
]
print(f"    {'#':>2}  {'max':<10}  {'L2':<10}")
print("    " + "-" * 25)
for j in range(10):
    print(f"    {j+1:>2}  {str(sobol_max[0][j]):<10}  {str(sobol_L2[0][j]):<10}")
