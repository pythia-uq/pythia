"""Tutorial 01 - Approximation of Functions with Polynomial Chaos."""

import numpy as np
import pythia as pt


def target_function(x: np.ndarray) -> np.ndarray:
    """Target function.

    Parameters
    ----------
    x : np.ndarray
    """
    f1 = np.sin(4 * np.pi * x) + 2
    f2 = -np.sin(3 * np.pi * x) + 2
    return np.column_stack((f1, f2))


print("Tutorial 01 - Approximation of Functions with Polynomial Chaos")

param = pt.parameter.Parameter(name="x", domain=(0, 1), distribution="uniform")

print("parameter information:")
print(param)

indices = np.arange(6, dtype=int).reshape(-1, 1)  # includes indices 0, ..., 5
index_set = pt.index.IndexSet(indices)  # try 15 for good a approximation
print("multiindex information:")
print(f"    number of indices: {index_set.shape[0]}")
print(f"    dimension: {index_set.shape[1]}")
print(f"    maximum dimension: {index_set.max}")
print(f"    number of sobol indices: {len(index_set.sobol_tuples)}")

N = 1000
print(f"generate training data ({N})")
s = pt.sampler.ParameterSampler([param])
x_train = s.sample(N)
w_train = np.ones(x_train.size) / x_train.size
y_train = target_function(x_train)

print("compute PC expansion")
surrogate = pt.chaos.PolynomialChaos([param], index_set, x_train, w_train, y_train)

N = 1000
print(f"generate test data ({N})")
x_test = s.sample(N)
y_test = target_function(x_test)
y_approx = surrogate.eval(x_test)

e_L2 = np.sqrt(np.sum((y_test - y_approx) ** 2) / y_test.shape[0])
e_L2_rel = e_L2 / np.sqrt(np.sum((y_test) ** 2) / y_test.shape[0])
e_max = np.max(np.abs(y_test - y_approx), axis=0)
e_max_rel = np.max(np.abs(y_test - y_approx) / np.abs(y_test), axis=0)
print(f"error L2 (abs/rel): {e_L2:4.2e}/{e_L2_rel:4.2e}")
print("error max (abs/rel):")
print(f"    first component:  {e_max[0]:4.2e}/{e_max_rel[0]:4.2e}")
print(f"    second component: {e_max[0]:4.2e}/{e_max_rel[0]:4.2e}")
