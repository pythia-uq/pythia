"""Automatic choice of expansion terms."""

from typing import Any

import numpy as np
import pythia as pt


def sobol_function(
    x: np.ndarray, a: np.ndarray | None = None, **kwargs: Any
) -> np.ndarray:
    """Sobol function.

    Parameters
    ----------
    x : np.ndarray
        Input values.
    a : np.ndarray | None, optional
        Coefficients, by default None

    Returns
    -------
    :
        Sobol function values.

    Raises
    ------
    ValueError
        Wrong dimension for input `x`.
    ValueError
        Wrong shape of Coefficients `a`.
    """
    if not 0 < x.ndim < 3:
        raise ValueError("Wrong ndim: {}".format(x.ndim))
    if x.ndim == 1:
        x.shape = 1, -1
    if a is None:
        a = np.zeros(x.shape[1])
    elif not a.shape == (x.shape[1],):
        raise ValueError("Wrong shape: {}".format(a.shape))
    return np.prod((abs(4.0 * x - 2.0) + a) / (1.0 + a), axis=1).reshape(-1, 1)


def sobol_sc(a: np.ndarray, dim: int = 1, **kwargs: Any) -> dict | np.ndarray:
    """Sobol function Sobol indices.

    Parameters
    ----------
    a : np.ndarray
        Coefficients.
    dim : int, optional
        Parameter dimension, by default 1.

    Returns
    -------
    :
        Sobol indices of Sobol function.
    """
    sobol = {}
    beta = (1.0 + a) ** (-2) / 3
    var = np.prod(1.0 + beta) - 1.0
    sobol_tuples = pt.index.IndexSet(pt.index.tensor_set([1] * len(a))).sobol_tuples
    for sdx in sobol_tuples:
        sobol[sdx] = 1.0 / var
        for k in sdx:
            sobol[sdx] *= beta[k - 1]
    if dim > 1:
        return np.array([sobol for _ in range(dim)])
    else:
        return sobol


print("Tutorial 04 - Automatic choice of expansion terms")

# function definitions
a = np.array([1, 2, 4, 8])


def target_function(x: np.ndarray) -> np.ndarray:
    """Target function.

    Parameters
    ----------
    x : np.ndarray
    """
    return sobol_function(x, a=a)


# analytical Sobol coefficients
sobol_dict = sobol_sc(a=a, dim=len(a))[0]
sobol_coefficients = np.array(list(sobol_dict.values())).reshape(-1, 1)

# setup reference PC surrogate
params = [
    pt.parameter.Parameter(name=f"x_{j+1}", domain=(0, 1), distribution="uniform")
    for j in range(a.size)
]

dim = 7
indices = pt.index.simplex_set(len(params), dim - 1)
index_set = pt.index.IndexSet(indices)
print("multiindex information:")
print(f"    number of indices: {index_set.shape[0]}")
print(f"    dimension: {index_set.shape[1]}")
print(f"    number of sobol indices: {len(index_set.sobol_tuples)}")

N = 10_000
print(f"generate training data ({N})")
s = pt.least_squares_sampler.OWLSTensorSampler(params, [dim] * len(params))
x_train = s.sample(N)
w_train = s.weight(x_train)
y_train = target_function(x_train)

print("compute pc expansion")
surrogate_ref = pt.chaos.PolynomialChaos(params, index_set, x_train, w_train, y_train)

# auto-generate PC multiindices
max_terms = index_set.shape[0] // 2
threshold = 1.0e-03
print("compute optimal mdx")
indices, sC = pt.chaos.find_optimal_indices(
    params, x_train, w_train, y_train, max_terms=max_terms, threshold=threshold
)
index_set = pt.index.IndexSet(indices)
print("automatic multiindex information:")
print(f"    number of indices: {index_set.shape[0]}")
print(f"    dimension: {index_set.shape[1]}")
print(f"    number of sobol indices: {len(index_set.sobol_tuples)}")

print("compute optimal pc expansion")
surrogate = pt.chaos.PolynomialChaos(params, index_set, x_train, w_train, y_train)

# test PC approximation
N = 1000
print(f"generate test data ({N})")
s_test = pt.sampler.ParameterSampler(params)
x_test = s_test.sample(N)
f_test = target_function(x_test)
f_approx = surrogate.eval(x_test)
f_approx_ref = surrogate_ref.eval(x_test)

error_L2_ref = np.sqrt(np.sum((f_test - f_approx_ref) ** 2) / x_test.shape[0])
error_L2 = np.sqrt(np.sum((f_test - f_approx) ** 2) / x_test.shape[0])
print(f"test error L2 auto: {error_L2:4.2e}")
print(f"test error L2 ref:  {error_L2_ref:4.2e}")

# print Sobol coefficients
print("Comparison of Sobol indices")
print(
    f" {'sobol_tuples':<13} {'exact':<8}  {'pc-auto':<8}  {'(crude)':<8}  {'pc-ref':<8}"
)
print("-" * 54)
for j, sdx in enumerate(index_set.sobol_tuples):
    print(
        f" {str(sdx):<12} ",  # Sobol index subscripts
        f"{sobol_coefficients[j, 0]:<4.2e} ",
        f"{surrogate.sobol[j, 0]:<4.2e} ",
        f"{sC[j][0]:<4.2e} ",
        f"{surrogate_ref.sobol[j, 0]:<4.2e} ",
    )
