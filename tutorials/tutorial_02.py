"""Tutorial 02 - Approximation of n-D functions with Polynomial Chaos."""

import numpy as np
import pythia as pt


def target_function(x: np.ndarray) -> np.ndarray:
    """Target function.

    Parameters
    ----------
    x : np.ndarray
    """
    val = -np.sin(4 * np.pi * x[:, 0]) * np.sin(3 * np.pi * x[:, 1]) + 2
    return val.reshape(-1, 1)


print("Tutorial 02 - Approximation of n-D functions with Polynomial Chaos")

print("set parameters")
param1 = pt.parameter.Parameter(name="x_1", domain=(0, 1), distribution="uniform")
param2 = pt.parameter.Parameter(name="x_2", domain=(0, 1), distribution="uniform")
params = [param1, param2]

sdim = [13, 11]  # stochastic dimensions (tensor)
indices = pt.index.tensor_set(sdim)
index_set = pt.index.IndexSet(indices)
print("multiindex information:")
print(f"    number of indices: {index_set.shape[0]}")
print(f"    dimension: {index_set.shape[1]}")
print(f"    maximum dimension: {index_set.max}")
print(f"    number of sobol indices: {len(index_set.sobol_tuples)}")

N = 1000
print(f"generate training data ({N})")
s = pt.least_squares_sampler.OWLSTensorSampler(params, sdim)
x_train = s.sample(N)
w_train = s.weight(x_train)
y_train = target_function(x_train)

print("compute pc expansion")
surrogate = pt.chaos.PolynomialChaos(params, index_set, x_train, w_train, y_train)

N = 1000
print(f"generate test data ({N})")
test_sampler = pt.sampler.ParameterSampler(params)
x_test = test_sampler.sample(N)
y_test = target_function(x_test)
y_approx = surrogate.eval(x_test)

# error computation
e_L2 = np.sqrt(np.sum((y_test - y_approx) ** 2) / x_test.shape[0])
e_L2_rel = e_L2 / np.sqrt(np.sum((y_test) ** 2) / x_test.shape[0])
e_max = np.max(np.abs(y_test - y_approx), axis=0)
e_max_rel = np.max(np.abs(y_test - y_approx) / np.abs(y_test), axis=0)
print(f"error L2 (abs/rel):  {e_L2:4.2e}/{e_L2_rel:4.2e}")
print(f"error max (abs/rel): {e_max[0]:4.2e}/{e_max_rel[0]:4.2e}")
