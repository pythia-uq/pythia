# Developing PyThia

- [Development Setup](#setup)
- [Running Tests](#tests)
- [Coding Rules](#rules)
- [Git Workflow](#gitflow)
- [Commit Message Guidelines](#commits)
- [Writing Documentation](#documentation)
- [Release Versioning](#versioning)

## <a name="setup"> Development Setup

This document describes how to set up your development environment to build and test PyThia, and explains the basic mechanics of using `git`.

### Forking PyThia on GitLab

To contribute code to PyThia, you must have an account for the PTB GitLab, so you can push code to your own fork of PyThia and open merge requests in the [GitLab Repository][gitlab].

### Installing Development Environment

You can install all required packages using
This installs packages for running source code, linting, testing, building the documentation and uploading to `PyPi.org``.
```shell
pip install -r requirements.txt
```
You can also install a development environment called `pythia-dev` using `conda` by
```shell
conda env create --file environment.yml
```
You should also add the repository root to you local dev environment by running
```shell
conda develop .
```

### Building PyThia

To build PyThia, you clone the source code repository and use `pip` to install the package.
The package's only requirements are `numpy` and `scipy` and `psutil`.

```shell
# Clone your GitLab repository:
git clone https://gitlab.com/<gitlab username>/pythia.git

# Go to the PyThia directory:
cd pythia

# Add the main PyThia repository as an upstream remote to your repository:
git remote add upstream "https://gitlab1.ptb.de/pythia/pythia.git"

# Build PyThia:
pip install .
```

### Updating stable Version on PyPi.org

If you want to make a new stable version of the PyThia package available through `pip` installation, checkout the branch and commit you want to create a wheel to upload from.
You can checkout the tag with
```shell
git fetch --all --tags
git checkout tags/vX.X.X -b vX.X.X-branch
```

**Note:** Make sure that the version in `setup.py` and `pythia/__init__.py` is correct and consistent.
Also check if the `CITATION.cff` file and the BibTex entries in `README.md` and `docs/source/index.rst` are up-to-date.
Don't forget to update the `CHANGELOG.md`.

Install/update `build` and `twine` via
```shell
pip install --upgrade build
pip install --upgrade twine
```
Then, in the root of the `pythia` repository, run
```shell
python -m build
```
to create a wheel for the current version under `dist/`.
To upload the wheel to ![PyPI.org](https://pypi.org/), use
```shell
python -m twine upload dist/*
```
and enter the following credentials:
  - **user name:** `__token__` (yes the username is `__token__`!)
  - **password:** (API token, ask a maintainer if you have none)
The new version can then be installed by `pip install pythia-uq`.

## <a name="tests"> Running Tests

### <a name="unit-tests"></a> Running Lint Tests

To run linting test, use
```shell
python -m flake8 pythia/ tests/ tutorials/
python -m black pythia/ tests/ tutorials/
```

### <a name="unit-tests"></a> Run Unit Tests

Unit tests run automatically on all modules when pushing code to the ![PyThia repository](https://gitlab1.ptb.de/pythia/pythia).
If you want to run the unit tests manually, you can use the pytest module, i.e., run
```shell
python -m pytest . -v
```
in the repository root.

### <a name="integration-tests"></a> Run Integration Tests

Execute the files in the `tutorials/` directory to see if integration tests are running through.

## <a name="rules"></a> Coding Rules

To ensure consistency throughout the source code, keep these rules in mind as you are working:

- All features or bug fixes **must be tested**.
- All public API methods **must be documented**.
  To see how we document our APIs, please check out the existing source code and see the section about [writing documentation](#documentation)
- With the exceptions listed below, we follow the rules contained in [Google's Python Style Guide][py-style-guide]:
    - We **write scientific algorithms**, which is why we prefer to stay close the publications that describe the algorithms.
      This is why we find it better to use close-to-mathematical variable and functions names over the PEP-8 standard in those cases.
      The code should however be as readable as possible and contain clarifying context information as comments.
    - We **prefer readability over performance** as PyThia should provide an easy-to-use environment for uncertainty quantification.
      The mathematics is difficult enough to understand, so make it as easy as possible to see how the code is working.

## <a name="gitflow"></a> Git Workflow

In the PyThia project, we follow the general GitFlow approach:

<p align="center">
  <img src="http://www.cecilianatale.it/wp-content/uploads/2017/08/release_branch.png" width="750"/>
</p>

In particular this means that the `development` branch (Develop) is the default branch of the repository and implements all changes to the code.
To add features or fix bugs, create a new feature branch, e.g., `feature/add-cool-stuff` or `bugfix/make-this-work` and commit your changes to this branch.
If you think that you are ready, push your changes to the `development` branch to create a merge request.

The tip of the `main` branch (Main) should always be deployable, i.e., it should always contain a tested running version of the code.
To keep things clean, release commits in `main` are always given a specific realease tag.

**Note:** Do not squash commits when merging into `main` as this causes a divorce of the `main` and `development` histories and many merge conflicts on future releases.

Releases are typically temporary branches branched off of `development` (Release).
After everything for the release is tested, the branch is merged back into `development` and into `main`, before it is deleted.

## <a name="commits"></a> Git Commit Guidelines

We have very precise rules over how our git commit messages can be formatted.
This leads to **more readable messages** that are easy to follow when looking through the **project history**.
But also, we use the git commit messages to **generate the PyThia change log**.

The commit message formatting can be added in your terminal after staging your changes in git.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.

The **header** is mandatory and should not exceed 50 characters.

The **body** and **footer** are optional, however mentioning an issue in the footer will close the issue automatically.
Any line of the commit message cannot be longer than 72 characters!
This allows the message to be easier to read on GitLab as well as in various git tools.

Good practices for writing informative git commit messages can be found in [this cheat sheet][commit-message-cheat-sheet] and this [blog post][commit-message-blog-post].

### Header
The header contains succinct description of the change:
  - use the imperative, present tense: "change" not "changed" nor "changes"
  - don't capitalize first letter
  - no dot (.) at the end

### Body
Just as in the **header**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.
Use full sentences to describe the changes.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to reference [GitLab issues][gitlab-issues] that this commit closes.

## <a name="documentation"></a> Writing Documentation

The PyThia project uses the [NumPy Documentation Style](https://numpydoc.readthedocs.io/en/latest/format.html) for all of its code documentation.

This means that since we generate the documentation from the source code, we can easily provide version-specific documentation by simply checking out a version of PyThia and running the build.

Extracting the source code documentation, processing and building the docs is handled by the documentation generation tool [sphinx][sphinx].

### Building and viewing the docs locally

The documentation can be generated automatically using `sphinx`.
Assuming you have `pythia`, `sphinx` and the `sphinx_rtd_theme` installed, the documentation can be generated by
```shell
cd docs
make html
```
To view the documentation locally, open `docs/build/html/index.html` in a browser of your choice.

### General documentation with Markdown

Any text in tags can contain markdown syntax for formatting.
Generally, you can use any markdown feature.

## <a name="versioning"></a> Determining Release Versions

We use [semantic versioning][semver] for PyThia releases, that is e.g. `v1.0.3`.
Given a version number `MAJOR`.`MINOR`.`PATCH`, increment the:
  - `MAJOR` version when you make incompatible API changes,
  - `MINOR` version when you add functionality in a backwards compatible manner, and
  - `PATCH` version when you make backwards compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the `MAJOR`.`MINOR`.`PATCH` format.

[commit-message-cheat-sheet]: https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
[commit-message-blog-post]: http://who-t.blogspot.com/2009/12/on-commit-messages.html
[sphinx]: https://www.sphinx-doc.org/en/master/
[gitlab-issues]: https://gitlab1.ptb.de/farchm01/pythia/-/issues
[gitlab]: https://gitlab1.ptb.de/farchm01/pythia/
[py-style-guide]: https://google.github.io/styleguide/pyguide.html
[semver]: https://semver.org/
