Changelog
=========

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Valid types of change are:
  - `Added` for new features
  - `Changed` for changes in existing functionality
  - `Deprecated` for soon to be removed features
  - `Removed` for now removed features
  - `Fixed` for any bug fixes
  - `Security` in case of vulnerabilities

v4.0.2 (2023-09-08)
-------------------

### Added
    - zenodo release support

v4.0.1 (2023-09-08)
-------------------

### Changed
    - git workflow for releases

v4.0.0 (2023-09-08)
-------------------

### Added
    - linting tests to CI/CD
    - tutorial scripts
    - integration tests for tutorials
    - function documentation
    - function `index.set_difference`
    - function `index.lq_bound_set`

### Changed
    - dev setup now possible for pip and conda use
    - Python version to `>=3.10`
    - handling of partial derivatives in `chaos.PolynomialChaos.eval`
    - function names: 
        - `misc.shiftCoord -> misc.shift_coord`
        - `misc.cartProd -> misc.cart_prod`
        - `misc.formatTime -> misc.format_time`
        - `misc.wlsSamplingBound -> misc.wls_sampling_bound`
        - `misc.get_confidence_interval -> misc.confidence_interval`
        - `index.tensor -> index.tensor_set`
        - `index.simplex -> index.simplex_set`
        - `index.add_indices -> index.union`
        - `index.subtract_indices -> index.intersection`

### Removed
    - unneccesary test stages
    - index attribute of `parameter.Parameter`

### Fixed
    - overflow on computation of factorial

v3.1.0 (2023-04-21)
-------------------

### Added
    - auto-creating releases from tags
    - code coverage analysis
    - source files now include license information

### Changed
    - license: GPL 3.0 -> dual license LGPS 3.0+ or Hippocratic License 3.0

### Removed
    - deprecated code

v3.0.1 (2023-02-16)
-------------------

### Added
    - Funding information
    - CITATION.cff

### Changed
    - dev environment

v3.0.0 (2022-12-10)
-------------------

### Added
    - installation guide and tutorials (doc)
    - unit and integration tests in CI/CD pipeline
    - reference to pythia online doc and bibtex entry for citation (README)
    - unit tests for all modules
    - dev environment yml

### Changed
    - update doc for pythia installation
    - change class, method, attribute and function names to be more direct
    - update doc strings for all modules
    - simplify internal structure of modules for better accessibility of usage
    - renamed `parameter.RandomParameter` to `parameter.Parameter`
    - make `parameter.Parameter` to data class

### Fixed
    - doc typos
    - type hinting in doc
    - proxy settings for CI/CD pipeline

### Removed
    - `likelihood` module
    - `mcmc_sampling` module
    - `loggers` module
    - `density` module (superseeded by `sampler` module)
    - `parameter.Parameter`
    - `parameter.SteadyParameter`
    - `chaos.PolynomialChaosFrame`
    - depricated functions

v2.0.1 (2022-01-23)
-------------------

### Changed
    - Update doc for pythia installation

### Fixed
    - bug in RTD config file (doc)

v2.0.0 (2021-10-28)
-------------------

### Added
    - PyThia logo

### Changed
    - import structure within the package
    - module names now adhere snake_case naming conventions

v1.1.0 (2021-10-28)
-------------------

### Added
    - CHANGELOG.md
    - CONTRIBUTING.md
    - DEVELOPERS.md
    - Version navigation.
    - CI/CD (wip)

### Changed
    - add doc-strings to functions and classes and subpackages
    - README.md

### Fixed
    - NumPy sqrt and factorial issue

v1.1.0 (2021-10-28)
-------------------

### Added
    - LICENSE.txt GNU_GPL v3

### Changed
    - code format admits PEP-8 and NumPy Doc standards
