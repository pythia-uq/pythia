---
title: 'PyThia: A Python package for Uncertainty Quantification based on  non-intrusive polynomial chaos expansions'
tags:
  - Python
  - polynomial chaos expansion
  - sensitivity analysis
  - regression
  - function approximation
authors:
  - name: Nando Hegemann
    orcid: 0000-0003-3953-9006
    equal-contrib: true
    corresponding: true
    affiliation: 1
  - name: Sebastian Heidenreich
    orcid: 0000-0002-1909-5770
    equal-contrib: true
    affiliation: 1
affiliations:
 - name: Physikalisch-Technische Bundesanstalt, Germany
   index: 1
date: 21 April 2023
bibliography: paper.bib

---

# Summary

`PyThia` is a Python package for quantifying uncertainties by computing polynomial chaos surrogates for computationally expensive parametric models (e.g., parametric partial differential equations) and for performing a local and global sensitivity analysis of those parameters.
At its core, `PyThia` aims to be close to mathematical foundations, general in its applications, and easy to use by non-experts.
To achive broad applicability in many areas, `PyThia` relies on a non-intrusive regression approach [@sudret:2008;@farchmin:2019].
This allows it to compute global polynomial approximations of complex physical models by accessing samples of inputs and outputs alone.
In particular, this means that `PyThia` can be used without any adaptation of legacy software, independent of operating system, programming language, involved proprietary software, or computing device.

`PyThia` covers polynomial chaos expansions for arbitrary combinations of uniform, normal, Gamma, and Beta distributed parameters.
The polynomial chaos expansion is handled in a sparse manner, meaning that the user can specify the desired expansion terms manually or automatically through a heuristic approach.
`PyThia` is able to compute a surrogate for any kind of input/output training data. However, as it is designed for high-dimensional inverse problems, random sampled data are typically preferred over approaches such as quadrature points or sparse grids.
Sample generation, according to a user-specified density or directly from the predefined parameter domains, is included in `PyThia` and can be used to create training inputs for the computationally expensive physical model of interest.
It is even possible to create input samples according to an optimal distribution with respect to the polynomial chaos expansion of the stochastic parameters [@migliorati:2017;@farchmin:2020].
The derived surrogate can then be used as an approximation to the physical model in parameter reconstruction tasks [@farchmin:2020] or for local and global sensitivity analysis [@sudret:2008;@farchmin:2019].
The latter comes without any computational overhead due to the use of an orthonormal polynomial basis in the chaos expansion.

# Statement of need

In many physical applications it is only possible to infer the quantities of interest indirectly, which often involves computationally expensive numerical simulations to solve the inverse problem [@andrle:2019;@hammerschmidt:2021].
As a way to circumvent this, surrogates to approximate the numerical model can be derived.
In particular, surrogates based on polynomial chaos expansions are very versatile as they can be used to approximate any function with a second moment [@xiu:2002], even in very high-dimensional spaces [@schwab:2012;@eigel:2014;@eigel:2023:avmc;@eigel:2023:exptt].
In contrast to similar model classes such as neural networks, it is possible to exploit the mathematical structure of the polynomial chaos expansion to compute moments (e.g., mean and variance), marginals, global parameter sensitivities [@sudret:2008], and arbitrary derivatives of the surrogate analytically without any computational overhead.
Additionally, the linear regression used to compute the polynomial chaos surrogate can be done in a non-intrusive way without the need to change the underlying numerical model by relying on function evaluations in randomly drawn samples.
Using the polynomial chaos basis, these samples can be drawn from an optimal distribution, which even yields upper bounds for the required number of function evaluations needed to compute the surrogate.

# Target Audience

`PyThia` is based on the `numpy` and `scipy` packages only and is thus very easy to install and run in almost any Python environment.
Moreover, to build a polynomial chaos surrogate of an arbitrary function, `PyThia` relies on a priori generated function evaluations.
This non-intrusive approach allows for maximal interoperability of `PyThia` with other code environments, i.e., different languages, machines and even proprietary software, as no interface between `PyThia` and the model computation framework has to be implemented.
In combination with the flexible choice of expansion multiindex set and the possibility to generate an optimal experimental design, `PyThia` provides an easy to use but still very flexible tool for arbitrary function approximation.
Hence the primary target audience for `PyThia` are experimental physicists and engineers, who often do not have the extensive mathematical background required to fine tune complicated algorithms and who typically face very intricate simulation frameworks which cannot be adapted easily.
For that reason `PyThia` has been utilized in meteorological applications ranging from shape reconstruction of circuit elements on photolithography masks [@farchmin:2019;@farchmin:2020;@casfor:2020] to sensitivity analysis for modeling electrocardiography signals in humans [@winkler:2022].

# State of the Field

Many software packages covering uncertainty quantification and function approximation using polynomial chaos are available both in python, e.g., [@pygpc;@chaospy;@openturns;@bayesvalidrox], and other programming languages, e.g., [@uqlab;@raven].

One of the main differences between `PyThia` and other software packages is the possibility to specify the multiindex set manually.
Typically, the multiindex set is either assembled directly or iteratively based on an $\ell_q$-bound of the multiindices [@luethen:2021].
The `pygpc` library also allows for direct (sparse) specification of the multiindex set, but does not contain any routines to assemble these sets.
Separating the creation of the multiindex set from the polynomial chaos approximation has the advantage that it is very accessible to test different types of surrogate model classes and provides a flexible interface for different schemes to choose an optimal set.
Exemplary, `PyThia` implements a heuristic to choose the index set based on an estimate of the Sobol indices, but other approaches such as LASSO [@tibshirani:1996], LARS [@efron:2004] or estimator based adaptive choices [@eigel:2023:avmc] are possible as well.

Another important feature concerns the experimental design for the polynomial chaos expansion.
As deterministic quadrature rules suffer from the curse of dimensionality, most libraries implement random or pseudo random sampling schemes for the polynomial chaos regression.
However, as random sampling typically yields an extensive amount of evaluations of a potentially expensive model, it is crucial to choose a sampling scheme which requires as least samples as possible.
To our knowledge `PyThia` is the only available software package providing the optimal weighted least-squares sampling scheme described in [@migliorati:2017], which gives provable optimal upper bounds for the required number of samples.

# Acknowledgements

The authors would like to acknowledge funding for the development of the PyThia UQ Toolbox from the German Central Innovation Program (ZIM) No. ZF4014017RR7 and the European project 20IND04-ATMOC.
This project (20IND04-ATMOC) has received funding from the EMPIR programme co-financed by the Participating States and from the European Union's Horizon 2020 research and innovation programme.

# References

