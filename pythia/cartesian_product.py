"""An implementation of the cartesian product."""

import numpy as np


def cartesian_product(*arrays: np.ndarray) -> np.ndarray:
    """Return the cartesian product of the flattened arrays.

    Let N[i] = np.size(arrays[i]) for every factor arrays[0], ..., arrays[n-1].
    The returned product satsifies the identity
        product.shape == (N[0] * N[1] * ... * N[n-1], n) and
    Let 0 <= j[i] < N[i] for all i = 0, ..., n-1 be arbitrary and J = np.prod(j), then
        product[J] == [arrays[0][j[0]], arrays[1][j[1]], ..., arrays[n-1][j[n-1]]] .

    Parameters
    ----------
    arrays[1], arrays[2], ..., arrays[n] : array_like
        Factors of the cartesian product.

    Returns
    -------
    :
        Cartesian product of the flattened factors. This is an array containing all
        possible combinations of the elements of the input arrays.
    """
    return np.stack(np.meshgrid(*arrays, indexing="ij"), axis=-1).reshape(
        -1, len(arrays)
    )
