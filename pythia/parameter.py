"""PyThia classes containing Parameter information."""

from dataclasses import dataclass
from typing import Optional, TypeAlias

import numpy as np

Real: TypeAlias = int | float | np.floating


@dataclass
class Parameter:
    """Class used for stochasic parameters.

    Parameters
    ----------
    name : str
        Parameter name.
    domain : array_like
        Supported domain of the parameter distribution.
    distribution : str
        Distribution identifier of the parameter.
    mean : float, default=None
        Mean of parameter probability.
    var : float, default=None
        Variance of parameter probability.
    alpha : float, default=None
        Alpha value of Beta and Gamma distribution.
    beta : float, default=None
        Beta value of Beta and Gamma distribution.
    """

    name: str
    domain: tuple[Real, Real]
    distribution: str
    mean: Optional[Real] = None
    var: Optional[Real] = None
    alpha: Optional[Real] = None
    beta: Optional[Real] = None

    def __post_init__(self) -> None:
        """Sanity checks."""
        assert isinstance(self.name, str)
        assert isinstance(self.distribution, str)

        self.domain = tuple(self.domain)  # type: ignore
        assert len(self.domain) == 2
        assert isinstance(self.domain[0], Real) and isinstance(self.domain[1], Real)

        if self.distribution == "uniform":
            assert self.mean is None
            assert self.var is None
            assert self.alpha is None
            assert self.beta is None
        elif self.distribution == "normal":
            assert isinstance(self.mean, Real)
            assert isinstance(self.var, Real)
            assert self.var > 0
            assert self.alpha is None
            assert self.beta is None
        elif self.distribution in ["gamma", "beta"]:
            assert self.mean is None
            assert self.var is None
            assert isinstance(self.alpha, Real)
            assert isinstance(self.beta, Real)
            assert self.alpha >= 0
            assert self.beta >= 0
        else:
            raise ValueError(f"Unknown distribution: '{self.distribution}'")
